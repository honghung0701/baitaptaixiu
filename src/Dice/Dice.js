import React, { Component } from 'react'
import bg_game from '../assets/bgGame.png'
import Result from './Result'
import Thedice from './Thedice'
import "./game.css"

export default class Dice extends Component {
  render() {
    return (
      <div
      className='bg_game'
      style={{
        backgroundImage: `url(${bg_game})`,
        width: "100vw",
        height: "100vh",
      }}>
         <Thedice />
         <Result />
      </div>
    )
  }
}
