import React, { Component } from 'react'
import { connect } from 'react-redux'
import { PLAY_GAME } from './redux/constant/diceConstant'

class Result extends Component {
  render() {
    return (
      <div className='text-center pt-5 display-5'>
        <button 
        onClick={this.props.handlePlayGame}
        className='btn btn-success'>
            <span className='display-4'>Play Game</span>
        </button>
        <h2 className='pt-5'>{this.props.totalNotify}</h2>
        <p className='pt-5'>Tổng số lần thắng: {this.props.sumWinGame}</p>
        <p>Tổng số lần chơi: {this.props.sumPlayGame}</p>
      </div>
      
    )
  }
}
let mapDispatchToProps = (dispatch) => {
    return {
        handlePlayGame: () => {
            dispatch({
                type: PLAY_GAME,
            })
        }
    }
}
let mapStateToProps = (state) => {
    return {
        sumPlayGame: state.diceReducer.sumPlayGame,
        sumWinGame: state.diceReducer.sumWinGame,
        totalNotify: state.diceReducer.totalNotify,
        choose: state.diceReducer.choose,
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(Result)


