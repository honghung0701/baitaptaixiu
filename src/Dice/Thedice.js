import React, { Component } from 'react'
import { connect } from 'react-redux'
import { CHOOSE, TAI, XIU } from './redux/constant/diceConstant';


export class Thedice extends Component {
    state = {
        active: null,
    }
    
    handleChange = (value) => {
        this.setState({ active: value});
    }
    renderListDice =() => {
        return this.props.diceArray.map((item,index) => {
            return <img src={item.img} key={index} 
            style={{
                width: "100px",
                margin: "10px",
                borderRadius: "8px",
            }}
            alt="" />
        })
    }
  render() {
    return (
        <div className='d-flex justify-content-between container pt-5'>
            <button style={
                {
                width: 150,
                height: 150,
                fontSize: 50,
                transform: `scale(${this.state.active==TAI?1.5:1})`
                }} 
                onClick={() => {
                    this.props.handleChangeActive(TAI)
                    this.handleChange(TAI)
                }}
                className='btn btn-danger'>Tài</button>
            <div>{this.renderListDice()}</div>
            <button style={
                {
                width: 150,
                height: 150,
                fontSize: 50,
                transform: `scale(${this.state.active==XIU?1.5:1})`
                }
            }
            onClick={() => {
                    this.props.handleChangeActive(XIU)
                    this.handleChange(XIU)
                }}
            className='btn btn-dark'>Xỉu</button>
        </div>
    )
  }
}

const mapStateToProps = (state) => ({
    diceArray: state.diceReducer.diceArray,
    // choose: state.diceArray.choose,
})

const mapDispatchToProps = (dispatch) => {
    return {
        handleChangeActive: (choose) => {
            let action = {
                type: CHOOSE,
                payload: choose,
            }
            dispatch(action);
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Thedice)

 