import { CHOOSE, PLAY_GAME, TAI, XIU } from "../constant/diceConstant";

let initialState = {
    diceArray: [
        {
            img: "./imgXucSac/1.png",
            value: 1,
        },
        {
            img: "./imgXucSac/1.png",
            value: 1,
        },
        {
            img: "./imgXucSac/1.png",
            value: 1,
        }
    ],
    choose: null,
    sumPlayGame: 0,
    sumWinGame: 0,
    totalNotify: null,
}
export const diceReducer =(state=initialState,{type,payload}) => {
    switch (type) {
        case PLAY_GAME: {
            state.sumPlayGame++;
            let newDiceArray=state.diceArray.map((item) => {
                let random = Math.floor(Math.random() * 6) + 1 ;
                return {
                    img: `./imgXucSac/${random}.png`,
                    value: random,
                }
            })
            let sumDice = newDiceArray.reduce((value,current) => {
                return value + current.value;
            },0)
            state.totalNotify = "0";
            if (sumDice >= 11 && state.choose === TAI) {
                state.totalNotify = "You Win";
                state.sumWinGame++;
            } else if (sumDice < 11 && state.choose === XIU){
                state.totalNotify = "You Win";
                state.sumWinGame++;
            } else {
                state.totalNotify = "You Lose";
            }
            state.diceArray = newDiceArray;
            return {...state}
        }
        case CHOOSE: {
            
            return { ...state, choose: payload}
        }
        
        
        default:
            return state;
    }
}