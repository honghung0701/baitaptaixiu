import { combineReducers } from "redux";
import { diceReducer } from "./diceReducer";

export const rootReducer_Dice = combineReducers({ diceReducer })